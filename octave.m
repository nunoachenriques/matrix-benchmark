% Copyright 2016 Nuno A. C. Henriques <nunoachenriques@gmail.com>
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% Description
% ===========
% Octave performance testing for machine learning specific case.
% Simplistic testing, just a time measured matrix multiplication
% (A' * B) for ninety nine runs.

m = 99;                      % iterations
x1 = 100;
x2 = 1000;
y1 = 100;
y2 = 1000;
x = rand(x1, x2); % matrix (x1, x2), random floats, uniform dist., [0, 1[
y = rand(y1, y2); % matrix (y1, y2), random floats, uniform dist., [0, 1[
r = zeros(m,1);              % time results
printf("Octave\n");
printf("Compute X(%d,%d)' * Y(%d,%d) %d times!\n", x1, x2, y1, y2, m);
printf("Time:   real\n");
for i = 1:m
  t = tic();                 % timer start
  calc = x' * y;             % calculates X'*Y
  r(i) = toc(t);             % time!
  printf("%2d | %f s\n", i, r(i));
endfor
% maximum of all rows by column
printf("Max:    %f s\n", max(r));
% mean of all rows by column
printf("Mean:   %f s\n", mean(r));
% median of all rows by column
printf("Median: %f s\n", median(r));
% minimum of all rows by column
printf("Min:    %f s\n", min(r));
