# Copyright 2016 Nuno A. C. Henriques <nunoachenriques@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Description
# ===========
# TensorFlow performance testing for machine learning specific case.
# Simplistic testing, just a time measured matrix multiplication
# (A' * B) for ninety nine runs.

import tensorflow as tf
import numpy as np
import time as ti

m = 99                          # iterations
x1 = 100
x2 = 1000
y1 = 100
y2 = 1000
x = tf.random_uniform((x1, x2)) # mat.(x1, x2), rand. floats, uni. dist., [0, 1[
y = tf.random_uniform((y1, y2)) # mat.(y1, y2), rand. floats, uni. dist., [0, 1[
r = []                          # time results
print("TensorFlow (Python)")
print("Compute X({0},{1})' * Y({2},{3}) {4} times!".format(x1, x2, y1, y2, m))
print("Time:   real")

for i in range(m):
    t = ti.time()
    calc = tf.matmul(x, y, transpose_a=True) # calculates X'*Y
    r.append(ti.time() - t)                  # time in seconds (float)
    print("{0:2d} | {1:f} s".format(i, r[i-1]));

# maximum of all rows by column
print("Max:    {0:f} s".format(max(r)));
# mean of all rows by column
print("Mean:   {0:f} s".format(np.mean(r)));
# median of all rows by column
print("Median: {0:f} s".format(np.median(r)));
# minimum of all rows by column
print("Min:    {0:f} s".format(min(r)));
