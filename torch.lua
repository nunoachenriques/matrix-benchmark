-- Copyright 2016 Nuno A. C. Henriques <nunoachenriques@gmail.com>
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- Description
-- ===========
-- Torch performance testing for machine learning specific case.
-- Simplistic testing, just a time measured matrix multiplication
-- (A' * B) for ninety nine runs.

require "torch"
require "string"

m = 99                       -- iterations
x1 = 100
x2 = 1000
y1 = 100
y2 = 1000
x = torch.rand(x1, x2) -- matrix (x1, x2), random floats, uniform dist., [0, 1[
y = torch.rand(y1, y2) -- matrix (y1, y2), random floats, uniform dist., [0, 1[
r = torch.Tensor(m,3):zero() -- time results
print("Torch (Lua)")
print(string.format("Compute X(%d,%d)' * Y(%d,%d) %d times!", x1, x2, y1, y2, m))
print("Time:   real   |    user   |    sys   ")
t = torch.Timer()
for i = 1, m do
   t:reset()                 -- timer start
   calc = x:t() * y          -- calculates X'*Y
   time = t:time()           -- time: real, user, sys!
   r[i][1] = time["real"]
   r[i][2] = time["user"]
   r[i][3] = time["sys"]
   print(string.format("%2d | %f s | %f s | %f s", i, r[i][1], r[i][2], r[i][3]))
end
max = torch.max(r,1)         -- maximum of all rows by column
print(string.format("Max:     | %f s | %f s | %f s", max[1][1], max[1][2], max[1][3]))
mean = torch.mean(r,1)       -- mean of all rows by column
print(string.format("Mean:    | %f s | %f s | %f s", mean[1][1], mean[1][2], mean[1][3]))
median = torch.median(r,1)   -- median of all rows by column
print(string.format("Median:  | %f s | %f s | %f s", median[1][1], median[1][2], median[1][3]))
min = torch.min(r,1)         -- minimum of all rows by column
print(string.format("Min:     | %f s | %f s | %f s", min[1][1], min[1][2], min[1][3]))
