[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

# Matrix benchmark

Octave, TensorFlow, and Torch performance testing for machine learning
specific case. Simplistic testing, just a time measured matrix
multiplication (A' * B) for ninety nine runs.

# Testing

How to run each script using a Linux Shell command-line.

## Octave

```shell
octave-cli -q octave.m
```

## TensorFlow

```shell
python3 -q -B tensorflow-python.py
```

## Torch

```shell
th torch.lua
```

# License

Copyright 2016 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
